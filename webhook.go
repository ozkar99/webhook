package main

import (
	"flag"
	"log"
	"net/http"
	"os/exec"

	"gitlab.com/ozkar99/middleware"
)

var bind string
var authPair string
var command string
var foreground bool

func init() {
	flag.StringVar(&bind, "b", ":9095", "The bind string for the http server.")
	flag.StringVar(&authPair, "a", "deploy:d3pl0y", "The auth string for the http server.")
	flag.StringVar(&command, "c", "./deploy.sh", "The command to run on deploy.")
	flag.BoolVar(&foreground, "f", false, "Waits for command to finish before returning 200OK.")
	flag.Parse()
}

func main() {
	http.Handle("/deploy",
		middleware.BasicAuth(http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				if r.Method == http.MethodGet {
					http.Error(w, "only POST is allowed", http.StatusNotFound)
					return
				}

				if foreground {
					deploy(w, r, command)
				} else {
					go deploy(w, r, command)
				}

				w.WriteHeader(http.StatusOK)
			}), authPair))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "only /deploy is allowed", http.StatusNotFound)
	})

	log.Println("Starting server...")
	http.ListenAndServe(bind, nil)
}

func deploy(w http.ResponseWriter, r *http.Request, command string) {
	log.Println("Starting deploy...")

	var out []byte
	var err error

	ref := r.URL.Query().Get("ref")
	if ref != "" {
		log.Println("Deploying Reference: ", ref)
		out, err = exec.Command(command, ref).Output()
	} else {
		out, err = exec.Command(command).Output()
	}

	if err != nil {
		log.Println("Deploy errored out: ", err)
	} else {
		log.Println(string(out))
		log.Println("Deploy finished.")
	}

}
