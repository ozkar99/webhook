# webhook

webhook is a simple go server that when recieving a post (with http basic auth) on `/deploy` runs a command
(usually `./deploy.sh`. but that can be changed).  

this is usefull for deploying via webhooks on ci systems.   

## installation
if you have go isntalled and configured you can simply run $ `go get -u gitlab.com/ozkar99/webhook` and webhook will be available on $GOPATH/bin

if you cannot install via go (for example in coreos), you can also download a pre-compiled binary for linux amd64 like this:

$ `curl -L "https://gitlab.com/ozkar99/webhook/-/jobs/artifacts/master/raw/dist/linux/amd64/webhook?job=build" --output webhook`

feel free to check artifacts for additional binaries on: https://gitlab.com/ozkar99/webhook/-/jobs/artifacts/master/browse?job=build

## configuration

check the [wiki](https://gitlab.com/ozkar99/webhook/wikis/home) for examples on the recommended setup and config files.


## usage
By default it will look for a `deploy.sh` where you ran the command but that can be changed via the options.  

### options
`-b` is the bind string for the server (default is `:9095`)  
`-a` especifies a `key:value` pair of `user:password` for http basic auth. (default `deploy:d3pl0y`)  
`-c` especifies the command to run. (default is `./deploy.sh`)  
`-f` waits for command to finish before returning OK (default is `false`)

and lastly you can also run `-h` or `--help` to print something similar to the above.  